﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS_DIGLAB.ViewModel
{
    public class DashboardItems
    {
    }

    public class StaffDasboardDonutIem
    {
        public int HonoredVisits { get; set; }
        public int CancelledAppointments { get; set; }
        public int UnhonoredAppointments { get; set; }
    }

    public class BarChartItem
    {
        public int y { get; set; }
        public string x { get; set; }
    }
}