﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS_DIGLAB.ViewModel
{
    public class PieItem
    {
        public double value { get; set; }
        public string label { get; set; }
    }
}