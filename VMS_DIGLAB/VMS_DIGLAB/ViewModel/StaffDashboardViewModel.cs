﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VMS_DIGLAB.Models;

namespace VMS_DIGLAB.ViewModel
{
    public class StaffDashboardViewModel : VisitorDetail
    {
        public int TotalVisitors { get; set; }
        public int TotalSignedInVisitorToday { get; set; }
        public int TotalAppointmentsToday { get; set; }
        public List<MonthlyStat> MonthlyStats { get; set; }

        public List<Visitor> MostRecentVisitors { get; set; }

    }

    public class VisitorDetail
    {
        public string VisitorName
        {
            get { return VisitorFirstName + " " + VisitorLastName; }
        }

        public string VisitorFirstName { get; set; }
        public string VisitorLastName { get; set; }
    }

    public class MonthlyStat
    {
        public string MonthName { get; set; }
        public int VisitorCount { get; set; }
    }
}