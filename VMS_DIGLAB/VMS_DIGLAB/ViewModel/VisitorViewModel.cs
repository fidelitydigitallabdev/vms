﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VMS_DIGLAB.Models;

namespace VMS_DIGLAB.ViewModel
{
    public class VisitorViewModel : VisitorDetail
    {
        public string VisitDate { get; set; }
        public string VisitTime { get; set; }

        public string VisitImage { get; set; }
        public byte[] VisitorImage { get; set; }


        public int? VisitStatus { get; set; }

        
        public bool IsAppointment { get; set; }

      
        public string VisitorCompany { get; set; }


        public string LaptopName { get; set; }

        public string SerialNumber { get; set; }
        public DateTime TimeIn { get; set; }

        
        public DateTime? TimeOut { get; set; }
        public string Email { get; set; }
  

        
        public string VisitorPhoneNumber { get; set; }

       
        public string BranchName { get; set; }

        public AppointmentPurpose Purpose { get; set; }
        public string VisitorFullName { get; internal set; }
    }
}