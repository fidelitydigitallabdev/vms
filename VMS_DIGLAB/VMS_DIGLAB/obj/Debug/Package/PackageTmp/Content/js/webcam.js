(function() {
    navigator.getMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia);

    navigator.getMedia(
        { video: true, audio: false },
        function (stream) {
            let video = document.getElementsByTagName('video')[0];
            video.src = window.URL.createObjectURL(stream);
            video.play();
        },
        function (error) {
            console.log(error);
        }
        document.getElementById("capture").addEventListener("click", takeSnapshot);
})() ;

function takeSnapshot() {
    let canvas = document.getElementById("capture");
    let video = document.getElementById("player");
    let image = doument.getElementById("output");

    width = video.videoWidth;
    height = video.videoHeight;

    context.drawImage(video, 0, 0, width, height);

    let imageDataURL = canvas.toDataURL('images/png');
    image.setAttribute('src', imageDataURL);
}