﻿/*---- morrisBar1----*/
$(function (e) {
    'use strict';
    new Morris.Area({
        element: 'morrisBar-chart',
        behaveLikeLine: true,
        data: [
            { x: '2018 Q1', y: 3, z: 0 },
            { x: '2018 Q2', y: 2, z: 3 },
            { x: '2018 Q3', y: 4, z: 2 },
            { x: '2018 Q4', y: 2, z: 4 },
            { x: '2018 Q5', y: 2, z: 1 },
            { x: '2018 Q6', y: 2, z: 4 },
            { x: '2018 Q7', y: 2, z: 0 },
            { x: '2018 Q8', y: 3, z: 3 }
        ],
        xkey: 'x',
        ykeys: ['y', 'z'],
        lineColors: ['#5C6BC0', 'rgb(129, 199, 132)'],
        labels: ['Y', 'Z']
    });
});

$(function (e) {

    $.ajax({
        url: "/vms_publish/receptionist/GetDashboardPieChartItems",
        type: 'GET',
        dataType: 'json',
        success: function (items) {
            console.log(items);
            if (Array.isArray(items)) {
                'use strict';
                new Morris.Donut({
                    element: 'morrisBar-pie',
                    data: items,
                    backgroundColor: '#fff',
                    labelColor: '#5e7cac',
                    colors: [
                        '#6574cd',
                        '#2bcbba',
                        '#f66d9b'

                    ],
                    formatter: function (x) { return x + "%" }
                });
            }
            else {
                alert("not an array");
            }

        }
    });

});

$(function (e) {

    $.ajax({
        url: "/vms_publish/receptionist/GetDashboarBarChartItems",
        type: 'GET',
        dataType: 'json',
        success: function (items) {
            console.log(items);
            if (Array.isArray(items)) {
                'use strict';
                new Morris.Bar({
                    element: 'morrisBar-graph',
                    
                    data: items,
                    xkey: 'x',
                    ykeys: ['y'],
                    labels: ['Y'],
                    barColors: function (row, series, type) {
                        if (type === 'bar') {
                            var red = Math.ceil(0 * row.y / this.ymax);
                            return 'rgb(63, 81, 181)';
                        }
                        else {
                            return '#000';
                        }
                    }
                });
            }
            else {
                alert("not an array");
            }

        }
    });

});


