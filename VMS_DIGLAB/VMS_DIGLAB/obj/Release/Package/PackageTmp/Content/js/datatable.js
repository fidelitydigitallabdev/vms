//Start of document ready fucntion
$(document).ready(function () {

    let checkedAppointment = false; let checkedDevice = false;//global variable
    let tab_num = 1;

    $('#myForm a').click(function (e) {
        e.preventDefault();
       // console.log("clicked");
        $(this).tab('show');
    });

    //function to check if there is an appointment
    $(".checkAppointment").click(function () {
        let check = $("input[name='appointment']:checked").val();
        if (check === "Yes") {
            checkedAppointment = true;
        } else checkedAppointment = false;
        if (checkedAppointment) {
            $('.form-group1').removeClass('hidden');
        } else $('.form-group1').addClass('hidden');
    })

    $('.checkDevice').click(function () {
        let check = $("input[name='device']:checked").val();
        if (check === "Yes") {
            checkedDevice = true;
        } else checkedDevice = false;
        if (checkedDevice) {
            $('.deviceFormGroup').removeClass('hidden');
        } else $('.deviceFormGroup').addClass('hidden');
    })
    //function on click of NEXT button
    $('.btnNext').click(navigate);

    function navigate() {
        
        let id = this.id;
        $('#' + this.id + 'Form').show();
        $('#btn0Form').hide();
        console.log(this.id + 'Form');
    }

}); //End of document ready function