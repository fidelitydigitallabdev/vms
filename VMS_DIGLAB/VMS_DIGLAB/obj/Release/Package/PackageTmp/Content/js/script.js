//Start of document ready fucntion
$(document).ready(function () {

    let checkedAppointment = false; let checkedDevice = false;//global variable
    //let tab_num = 1;

    //$('#myForm a').click(function (e) {
    //    e.preventDefault();
    //    //console.log(this.id);
    //    $(this).tab('show');
    //});

    //function to check if there is an appointment
    $(".checkAppointment").click(function () {
        let check = $("input[name='appointment']:checked").val();
        if (check === "Yes") {
            checkedAppointment = true;
        } else checkedAppointment = false;
        if (checkedAppointment) {
            $('.form-group1').removeClass('hidden');
        } else $('.form-group1').addClass('hidden');
    })

    $('.checkDevice').click(function () {
        let check = $("input[name='device']:checked").val();
        if (check === "Yes") {
            checkedDevice = true;
        } else checkedDevice = false;
        if (checkedDevice) {
            $('.deviceFormGroup').removeClass('hidden');
        } else $('.deviceFormGroup').addClass('hidden');
    })
    //function on click of NEXT button
    $('.btnNext').click(navigate);
    $('.btnPrev').click(navigateBack);

    function navigate() {
        
        let id = this.id;
        if (id === 'btn1') {
            $('#' + this.id + 'Form').show();
            $('#btn0Form').hide();
            $('#step1').removeClass('activeTab');
            $('#step2').addClass('activeTab');
        } else if (id === 'btn2') {
            $('#' + this.id + 'Form').show();
            $('#btn1Form').hide();
            $('#step2').removeClass('activeTab');
            $('#step3').addClass('activeTab');
        }
    }

    function navigateBack() {
        let id = this.id;
        if (id === 'btnP1') {
            $('#btn1Form').hide();
            $('#btn0Form').show();
            $('#step2').removeClass('activeTab');
            $('#step1').addClass('activeTab');
        } else if (id === 'btnP2') {
            $('#btn2Form').hide();
            $('#btn1Form').show();
            $('#step3').removeClass('activeTab');
            $('#step2').addClass('activeTab');
        }
        console.log(id);
    }

    //function for selecting a device or not
    
    $("select")
        .change(function () {
            var str = "";
            $("select option:selected").each(function () {
                str += $(this).text();
            });
            if (str === 'Yes') {
                $('.typeOfDevice').removeClass('hidden');
            } else $('.typeOfDevice').addClass('hidden');
            console.log(str);
        })

}); //End of document ready function