﻿using DigiFoodLabServiceCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VMSServiceCore;
using VMS_DIGLAB.Models;
namespace VMS_DIGLAB.Controllers
{
    public class VisitorsController : Controller
    {
       
        
        
        public ActionResult Create()
        {
            return View();
        }

        // POST: Visitors/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Visitor visitor)
        {
            var requestId = Utilities.RandomNumber(10000, 2435353).ToString();
            //write request to log
            ErrHandler.WriteLog($"User with ip {Utilities.GetIP()} attempts to create a new visitor", requestId);
            
            //If no details of the user, return a bad request
            if (visitor == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //if there are validation errors, modelstate will not be valid, hence return back the view to show the validation error
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                //call the CreateVisitor procedure
            }
            catch (Exception ex)
            {

                ErrHandler.WriteError($"Error creating the user : {ex.Message}", requestId);
            }
            

            return View();
        }

        // only get visitors of the currently logged in user -Role staff
        private List<Visitor> GetStaffVisitors(string staffId, DateTime from, DateTime to)
        {
            List<Visitor> visitor = new List<Visitor>();

            // run procedure to retrieve the staff visitors
            return visitor;
        }

        
        // get all visitors for a branch - Receptionist and admin Role 
        private List<Visitor> GetBranchVisistors(string staffId, DateTime from, DateTime to, string branchName)
        {
            List<Visitor> visitor = new List<Visitor>();

            // run procedure to retrieve the all staff visitors for a branch
            return visitor;
        }

        // get visitor detail  - Receptionist and admin Role
        private Visitor GetVisitorDetail(int visitorId)
        {
            Visitor visitor = new Visitor();

            //run procedure to return the visitor whose id was passed to retreive his details
            return visitor;
        }

        public  void SignoutVisitor(string visitorId)
        {
            //all logic to sign out visitor here
        }

        //only admin related can call this method
        private List<Visitor> GetAllVisitors(string staffId, DateTime from, DateTime to, string branchName)
        {
            List<Visitor> visitor = new List<Visitor>();

            // run procedure to retrieve the all staff visitors for a branch
            return visitor;
        }


    }
}
