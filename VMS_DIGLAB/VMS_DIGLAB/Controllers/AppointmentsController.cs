﻿using DigiFoodLabServiceCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VMS_DIGLAB.Models;
using VMSServiceCore;

namespace VMS_DIGLAB.Controllers
{
    [Authorize]
    public class AppointmentsController : Controller
    {
       
        public ActionResult Index()
        {
            //var staffId = "P7578";
            //List<Appointment> appointments = GetStaffAppointments(staffId);

            //return View(appointments);
            return View();
        }

        

        public ActionResult CreateAppointment(Appointment appointment)
        {
            var requestId = Utilities.RandomNumber(1000, 2435353).ToString();
            //write request to log
            ErrHandler.WriteLog($"User with ip {Utilities.GetIP()} attempts to create a new appointment", requestId);

            //If no details of the user, return a bad request
            if (appointment == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //if there are validation errors, modelstate will not be valid, hence return back the view to show the validation error
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                //call the CreateAppointment procedure
            }
            catch (Exception ex)
            {

                ErrHandler.WriteError($"Error creating the user : {ex.Message}", requestId);
            }
            return View();
        }

        // /appointments/details/1
        public JsonResult Details(int Id)
        {
            Appointment appointment = new Appointment
            {
                AppointmentDate = DateTime.Now,
                AppointmentId = Convert.ToInt32(Id),
                BranchName = "40, Awolowo Road, Ikoyi, Lagos",
                Purpose = AppointmentPurpose.Official,
                VisitorFirstName = "Aribo",
                VisitorLastName = "Oluwatoba",
                VisitorPhoneNumber = "09038969144" , 
                Host = new Staff
                {
                    FullName = "Gbenga Orisanusi", 
                     StaffId = "P7578", 
                }
                
            };

            return Json(appointment, "application/json", JsonRequestBehavior.AllowGet);
        }
        //[Route("appointments/details/{query}")]
        // // Only routes that is accessible to admin and receptionist can call this method || or accessible to the staff only if he owns the appointment
        public JsonResult GetBranchAppointees(string query)
        {
            object val = new object[]
            {
               new { value = "Mike Murdock", Id=1},
               new { value = "Shola Atkinson", Id=2},
               new { value = "Lanre McCaulay", Id=3},
               new { value = "Victor Monroe", Id=4},
               new { value = "Bull Skini", Id=5},
               new { value = "Joseph Sparrow", Id=6},
               new { value = "Vinil Text", Id=7},
               new { value = "Xerxes III", Id=8}
            };


            var result = Json(val, JsonRequestBehavior.AllowGet);
            //run your stored procedure to get appointments based on AppointmentID

            return result;
        }


        
        // Accessible to all roles, HOWEVER IF 
        public List<Appointment> GetStaffAppointments(string staffId)
        {
            List<Appointment> appointments = new List<Appointment>();

            try
            {

            }
            catch (Exception ex)
            {

                //ErrHandler.WriteLog();
            }

            //execute stored procedure to retreive the staff appointments


            return appointments;
        }

        // Accessible to all roles, HOWEVER IF 
        public List<Appointment> GetStaffAppointments(string staffId, DateTime from, DateTime to)
        {
            List<Appointment> appointments = new List<Appointment>();


            //execute stored procedure to retreive the staff appointments


            return appointments;
        }


        // Only routes that is accessible to admin and receptionist can call this method
        public List<Appointment> GetBranchAppointments(string branchName, DateTime from, DateTime to)
        {
            List<Appointment> appointments = new List<Appointment>();


            //execute stored procedure to retreive appointments for a specific branch


            return appointments;
        }

        //admin cannot cancel appointments. Take note
        public void CancelAppointment()
        {
            //logic here 
        }

        // Only call this for admin 
        public List<Appointment> GetAllAppointments(string branchName, DateTime from, DateTime to)
        {
            List<Appointment> appointments = new List<Appointment>();


            //execute stored procedure to retreive appointments for a specific branch


            return appointments;
        }
        
        public JsonResult Detail(int id)
        {
            ErrHandler.WriteLog("/appointments/detail/"+id, Utilities.GetIP());
            Appointment appointment = new Appointment();
            try
            {
                var resp = Utilities.EnCapriDisco_Select(id, "", "GetAppointmentDetailsByAppointmentId", "@AppointmentId");

                if (resp.Rows.Count == 0)
                {

                }
                

                foreach (DataRow row in resp.Rows)
                {

                    appointment.BranchName = row["BranchName"].ToString();
                    appointment.VisitorCompany = row["VisitorCompany"].ToString();
                    appointment.Email = row["Email"].ToString();
                    appointment.VisitorFirstName = row["VisitorFirstName"].ToString();
                    appointment.VisitorLastName = row["VisitorLastName"].ToString();
                    appointment.ArrivalDate = row.Field<DateTime?>("AppointmentTime").Value.ToShortDateString();
                    appointment.ArrivalTime = row.Field<DateTime?>("AppointmentTime").Value.ToShortTimeString();
                    appointment.Purpose = (AppointmentPurpose)row.Field<int>("Purpose");//Convert.ToInt32(row["Purpose"].ToString());
                    appointment.VisitorPhoneNumber = row["VisitorPhoneNumber"].ToString();
                    appointment.AppointmentId = row.Field<int>("AppointmentId");
                }

                
            }
            catch (Exception ex)
            {
                ErrHandler.WriteLog("appointments/detail/" + id+ " "+ ex.Message, Utilities.GetIP());

            }
            return Json(appointment, JsonRequestBehavior.AllowGet);
        }

        [Route("appointments/modify/{id}")]

        public JsonResult Modify(Appointment appointment, string id)
        {
            //check if the owner of the appointment is the currently logged in user
            string code = "xx";
            string message = string.Empty;
            try
            {

                IFormatProvider culture = new CultureInfo("en-US", true);
                
                DateTime appointmentDate = DateTime.Parse(appointment.ArrivalDate);
                DateTime appointmentTime = DateTime.Parse(appointment.ArrivalDate + ' ' + appointment.ArrivalTime);
                var timeOfDay = appointmentTime.TimeOfDay;
                TimeSpan startTime = new TimeSpan(8, 0, 0);
                TimeSpan endTime = new TimeSpan(16, 59, 0);
                
                //check the day it falls in and carry out appropraite action
                DayOfWeek dayOfAppointment = appointmentTime.DayOfWeek;

                if (dayOfAppointment == DayOfWeek.Sunday || dayOfAppointment == DayOfWeek.Saturday)
                {
                    code = "99";
                    message = "Can't modify an appointment to a Saturday or Sunday";

                }
                else if (appointmentTime < DateTime.Now) //if appointment is past
                {
                    code = "99";
                    message = "Can't modify appointment to an earlier date.";
                }
                else if(!(timeOfDay >= startTime && timeOfDay <= endTime)) 
                {
                    code = "99";
                    message = "Can't modify appointment to that hour chosen.";
                }
                else  //if not on a saturday or sunday
                {

                    Dictionary<string, dynamic> sqlParameters = new Dictionary<string, dynamic>();

                    sqlParameters.Add("@VisitorFirstName", appointment.VisitorFullName);
                    sqlParameters.Add("@VisitorLastName", appointment.VisitorFullName);
                    sqlParameters.Add("@VisitorPhoneNumber", appointment.VisitorPhoneNumber);

                    sqlParameters.Add("@AppointmentId", id);
                    sqlParameters.Add("@Email", appointment.Email);
                    sqlParameters.Add("@VisitorCompany", appointment.VisitorCompany);
                    sqlParameters.Add("@AppointmentDate", DateTime.Parse(appointment.ArrivalDate)); //convert this date '02-2-2019'
                    sqlParameters.Add("@AppointmentTime", DateTime.Parse(appointment.ArrivalDate + ' ' + appointment.ArrivalTime));  //'10:50'
                    sqlParameters.Add("@Purpose", (AppointmentPurpose)appointment.Purpose);
                    // use this line to show it back as 24hr format 
                    //var hourPart = String.Format("{0:HH}", myDateTime)



                    int result = Utilities.EnCapriDisco_Insert("ModifyAppointment", sqlParameters, $"appointments/modify/{id}", Utilities.GetIP());
                    if (Math.Abs(result) > 0)
                    {
                        code = "00";
                        message = "Your appointment was modified successfully";
                    }
                    else
                    {
                        code = "99";
                        message = "An error occured modifying the appointment. Please try again later!";
                    }

                }

            }
            catch (Exception ex)
            {

                ErrHandler.WriteError("Error occured updating trying to update the database", ex.Message);
                code = "99";
                message = "Error occured modifying the appointment. Please try again later!";
            }
           

            
            
            return Json(new { code, message }, JsonRequestBehavior.AllowGet);
        }
        [Route("appointments/cancel/{appointmentId}")]
        public JsonResult Cancel(int appointmentId)
        {
            //check if the owner is the owner of the appointment and the status
            string code = "xx";
            try
            {

                Dictionary<string, dynamic> sqlParameters = new Dictionary<string, dynamic>();

                sqlParameters.Add("@AppointmentId", appointmentId);
                sqlParameters.Add("@AppointmentAction", 3);
          
                // use this line to show it back as 24hr format 
                //var hourPart = String.Format("{0:HH}", myDateTime)



                int result = Utilities.EnCapriDisco_Insert("ChangeAppointmentStatus", sqlParameters, $"appointments/cancel/{appointmentId}", Utilities.GetIP());
                if (Math.Abs(result) > 0)
                {
                    code = "00";
                }
                else
                {
                    code = "99";
                }
            }
            catch (Exception ex)
            {

                ErrHandler.WriteError("Error occured updating trying to update the database", ex.Message);
            }

            return Json(new { code }, JsonRequestBehavior.AllowGet);
        }

       
        public JsonResult SignedIn(int appointmentId)
        {
            //check if the owner is the owner of the appointment and the status
            string code = "xx";
            try
            {

                Dictionary<string, dynamic> sqlParameters = new Dictionary<string, dynamic>();

                sqlParameters.Add("@AppointmentId", appointmentId);
                sqlParameters.Add("@AppointmentAction", 2);

                int result = Utilities.EnCapriDisco_Insert("ChangeAppointmentStatus", sqlParameters, $"appointments/cancel/{appointmentId}", Utilities.GetIP());
                if (Math.Abs(result) > 0)
                {
                    code = "00";
                }
                else
                {
                    code = "99";
                }
            }
            catch (Exception ex)
            {

                ErrHandler.WriteError("Error occured updating trying to update the database", ex.Message);
            }

            return Json(new { code }, JsonRequestBehavior.AllowGet);
        }
    }
}
