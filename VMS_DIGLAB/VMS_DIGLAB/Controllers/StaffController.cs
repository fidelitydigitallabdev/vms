﻿using DigiFoodLabServiceCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;
using VMS_DIGLAB.Models;
using VMSServiceCore;
using VMS_DIGLAB.ViewModel;
using System.Data;
using System.Text;
using System.Globalization;
using VMS_DIGLAB.Filters;
using System.Threading.Tasks;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using System.Threading;

namespace VMS_DIGLAB.Controllers
{
    [System.Web.Mvc.Authorize]
    [FidelityStaff]
    [ApplicationVariables]
    public class StaffController : Controller
    {
        public string staffId
        {
            get
            {
                return Session["StaffId"].ToString();
            }
        }
        public string fullName
        {
            get
            {
                return Session["FullName"].ToString();
            }
        }
        public StaffController()
        {
            //IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;
            //string email = user.UserPrincipalName;
            
            //staffId = email.Split(new Char[] { '@'})[0];
            //fullName = user.DisplayName;
        }
        private readonly ApplicationDbContext db = new ApplicationDbContext();
        // GET: Staff
        public ActionResult Index()
        {
            return RedirectToAction("DashBoard");
        }

        public ActionResult DashBoard()
        {
            //string staffId = System.Web.HttpContext.Current.Request.LogonUserIdentity.Name.Substring(13);
            

            StaffDashboardViewModel dashBoardItems = new StaffDashboardViewModel();
            try
            {
                //get total visitor count
                string userObjectID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;

                var totalVisitors = Utilities.EnCapriDisco_Select(staffId, "", "GetTotalStaffVisitorsByStaffId", "@staffid");

                if (totalVisitors.Rows.Count > 0)
                {
                    foreach (DataRow row in totalVisitors.Rows)
                    {
                        dashBoardItems.TotalVisitors = Convert.ToInt32(row["TotalVisitors"].ToString());
                    }
                }
                else
                {
                    dashBoardItems.TotalVisitors = 0;
                }

                //get most recent staff visitors
                Dictionary<string, dynamic> sqlParameters = new Dictionary<string, dynamic>();
                sqlParameters.Add("@staffid", staffId);
                sqlParameters.Add("@count", 5);

                var mostRecentVisitors = Utilities.EnCapriDisco_Select2("GetMostRecentVisitorsByStaffId", sqlParameters, "/staff/dashboard", staffId);
                List<Visitor> visitors = new List<Visitor>();
                if (mostRecentVisitors.Rows.Count > 0)
                {
                    

                    foreach(DataRow row in mostRecentVisitors.Rows)
                    {
                        visitors.Add(
                            new Visitor
                            {
                                Email = row["Email"].ToString(),
                                VisitorFullName = row["VisitorFullName"].ToString(),
                                //VisitorLastName = row["VisitorLastName"].ToString()
                                VisitImage = row["VisitImage"].ToString()
                            }
                        );
                    }
                    
                }
                dashBoardItems.MostRecentVisitors = visitors;

                //get total visitors signed in today
                var totalSignedInVistorsToday = Utilities.EnCapriDisco_Select(staffId, "", "GetTotalStaffVisitorsSignedInTodayByStaffID", "@staffid");

                if (totalVisitors.Rows.Count > 0)
                {
                    foreach (DataRow row in totalSignedInVistorsToday.Rows)
                    {
                        dashBoardItems.TotalSignedInVisitorToday = Convert.ToInt32(row["StaffVisitorCount"].ToString());
                    }
                }
                

                //get total appointments today
                DataTable totalAppointmentsToday = Utilities.EnCapriDisco_Select(staffId, "", "GetTotalStaffVisitorsOnAppointmentTodayByStaffID", "@staffid");

                if (totalAppointmentsToday.Rows.Count > 0)
                {
                    foreach (DataRow row in totalAppointmentsToday.Rows)
                    {
                        dashBoardItems.TotalAppointmentsToday = Convert.ToInt32(row["StaffVisitorCount"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrHandler.WriteError("Error getting dashboard items", ex.Message+"|"+Utilities.GetIP());
            }

            

            
            return View(dashBoardItems);
        }

        // GET: Staff
        public ActionResult Appointments()
        {
            string classMethod = "Appointments";
            //string staffId = System.Web.HttpContext.Current.Request.LogonUserIdentity.Name.Substring(13);
            
            string userObjectID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;

            List<Appointment> result = new List<Appointment>(); 
            
            try
            {
                result = db.Database.SqlQuery<Appointment>("exec GetStaffAppointments @StaffId", new SqlParameter("@StaffId", staffId)).ToList();
                var count = result.Count;

            }
            catch (Exception ex)
            {
                //return an error page
                ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, staffId);
            }
            

            //all logic to get staff appointments and return to the view
            return View(result);
        }

        // GET: Staff/CreateInvite
        public ActionResult CreateInvite()
        {
           
            Appointment appointment = new Appointment();
            appointment.Host = new Staff { FullName = fullName };
            //This shows the Create invite page
            return View(appointment);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        // POST: Staff/CreateInvite
        public ActionResult CreateInvite(Appointment appointment)
        {
            //string staffId = System.Web.HttpContext.Current.Request.LogonUserIdentity.Name.Substring(13);
            string classMethod = "/staff/createinvite";
            
            IFormatProvider culture = new CultureInfo("en-US", true);

            //string userObjectID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;

            if (ModelState.IsValid) //check modelstate is valid
            {
                
                DateTime tempDate;
                bool isValidDate = DateTime.TryParseExact(appointment.ArrivalDate, "dd-MM-yyyy",  culture, System.Globalization.DateTimeStyles.None, out tempDate);
                if (!isValidDate) //check if date is valid
                {
                    ViewBag.Status = "99";
                    ViewBag.ErrMessage = "Can't create an appointment. Invalid Date format";
                }
                else if (DateTime.ParseExact(appointment.ArrivalDate + ' ' + appointment.ArrivalTime, "dd-MM-yyyy HH:mm", culture) < DateTime.Now) //if appointment is past
                {
                    ViewBag.Status = "99";
                    ViewBag.ErrMessage = "Can't create an appointment for an earlier date.";
                }
                else //date is valid
                {
                    //convert the string to a standard datetime object
                    DateTime appointmentDate = DateTime.ParseExact(appointment.ArrivalDate, "dd-MM-yyyy", culture);
                    DateTime appointmentTime = DateTime.ParseExact(appointment.ArrivalDate + ' ' + appointment.ArrivalTime, "dd-MM-yyyy HH:mm" , culture);

                    //check the day it falls in and carry out appropraite action
                    DayOfWeek dayOfAppointment = appointmentTime.DayOfWeek;
                    if (dayOfAppointment == DayOfWeek.Sunday || dayOfAppointment == DayOfWeek.Saturday)
                    {
                        ViewBag.Status = "99";
                        ViewBag.ErrMessage = "Couldn't create an appointment on a staurday or sunday. Please try again";

                    }
                    else
                    {
                        try
                        {
                            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                            TextInfo textInfo = cultureInfo.TextInfo;

                            Dictionary<string, dynamic> sqlParameters = new Dictionary<string, dynamic>();
                            sqlParameters.Add("@StaffId", staffId);
                            sqlParameters.Add("@VisitorFirstName", textInfo.ToTitleCase(appointment.VisitorFullName)); 
                            sqlParameters.Add("@VisitorLastName", textInfo.ToTitleCase(appointment.VisitorFullName) );
                            sqlParameters.Add("@VisitorPhoneNumber", appointment.VisitorPhoneNumber);
                            sqlParameters.Add("@BranchName", appointment.BranchName);
                            sqlParameters.Add("@Purpose", appointment.Purpose);
                            sqlParameters.Add("@VisitorCompany", textInfo.ToTitleCase(appointment.VisitorCompany)) ;
                            sqlParameters.Add("@AppointmentDate", appointmentDate); //convert this date '02-2-2019'

                            sqlParameters.Add("@AppointmentTime", appointmentTime);  //'10:50'
                                                                                     //yyyy-MM-dd
                            int result = Utilities.EnCapriDisco_Insert("CreateAppointment", sqlParameters, classMethod, staffId);


                            if (Math.Abs(result) > 0)
                            {
                                TempData["Status"] = "00";
                                TempData["Message"] = "Appointment was created successfully!";
                                return RedirectToAction("Appointments", "Staff");
                            }
                            else
                            {
                                ViewBag.Status = "99";
                                ViewBag.ErrMessage = "Couldn't create an appointment. Please try again";
                            }

                        }
                        catch (Exception ex)
                        {
                            //return an error page
                            ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, "");
                            ViewBag.Status = "99";
                            ViewBag.ErrMessage = ex.Message;
                        }
                    }
                }
                
                

            }
            else
            {
                StringBuilder strError = new StringBuilder();
                var errors = ModelState.Values.SelectMany(x => x.Errors);

                if (errors.Any())
                {
                    string error = string.Empty;
                    foreach (ModelError item in errors)
                    {
                        error = string.IsNullOrEmpty(item.ErrorMessage) ? item.Exception.Message : item.ErrorMessage;
                        strError.Append(error + '|');
                    }
                   
                }
                ErrHandler.WriteError(classMethod + " Invalid model for creating appointments", strError.ToString().TrimEnd('|'));
                ViewBag.Status = "XX";
                ViewBag.ErrMessage = strError;
            }
            appointment.Host = new Staff { FullName = fullName };
            //logic to create new appointment here. Handle posted data here on the CreateInvitePage
            return View(appointment);
        }

        public ActionResult VisitorLog()
        {
            //string staffId = System.Web.HttpContext.Current.Request.LogonUserIdentity.Name.Substring(13);
            string classMethod = "/Staff/VisitorLog";
            List<VisitorViewModel> visitors = new List<VisitorViewModel>();

            string userObjectID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;

            ErrHandler.WriteLog("User is about to view all his visitor records", staffId);

            List<VisitorViewModel> result = new List<VisitorViewModel>();

            try
            {
                var resp = Utilities.EnCapriDisco_Select(staffId, "", "GetStaffVisitors", "@StaffId");

                if (resp.Rows.Count == 0 )
                {
                    
                }

               

                foreach (DataRow row in resp.Rows)
                {
                    visitors.Add(new VisitorViewModel
                    {

                        BranchName = row["BranchName"].ToString(),
                        VisitorCompany = row["VisitorCompany"].ToString(),
                        Email = row["Email"].ToString(),
                        VisitorFirstName = row["VisitorFullName"].ToString(),
                        
                        VisitorPhoneNumber = row["VisitorPhoneNumber"].ToString(),
                        TimeIn = row.Field<DateTime>("TimeIn"),
                        TimeOut = row.Field<DateTime?>("TimeOut"),
                        Purpose = (AppointmentPurpose)Convert.ToInt32(row["Purpose"].ToString()), 
                        VisitImage = row["VisitImage"].ToString()
                    });
                }




                var count = result.Count;
            }
            catch (Exception ex)
            {
                //return an error page
                ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, "");
            }

            return View(visitors);
        }


        public List<VisitorViewModel> GetVisitors(string staffId)
        {

            List<VisitorViewModel> visitors = new List<VisitorViewModel>();

            try
            {
                var resp = Utilities.EnCapriDisco_Select(staffId, "", "GetStaffVisitors", "@StaffId");

                if (resp.Rows.Count == 0)
                {

                }



                foreach (DataRow row in resp.Rows)
                {
                    visitors.Add(new VisitorViewModel
                    {

                        BranchName = row["BranchName"].ToString(),
                        VisitorCompany = row["VisitorCompany"].ToString(),
                        Email = row["Email"].ToString(),
                        VisitorFirstName = row["VisitorFirstName"].ToString(),
                        VisitorLastName = row["VisitorLastName"].ToString(),
                        VisitorPhoneNumber = row["VisitorPhoneNumber"].ToString(),
                        TimeIn = row.Field<DateTime>("TimeIn"),
                        TimeOut = row.Field<DateTime?>("TimeOut"),
                        Purpose = (AppointmentPurpose)Convert.ToInt32(row["Purpose"].ToString())
                    });
                }
            }
            catch (Exception ex)
            {

            }

            return visitors;
        }


        [System.Web.Mvc.Route("staff/visitors")]
        public JsonResult GetVisitorLog(string fromDate, string toDate)
        {
            string userObjectID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;
            //string staffId = System.Web.HttpContext.Current.Request.LogonUserIdentity.Name.Substring(13);

            var result = GetVisitors(staffId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDashboardPieChartItems()
        {
            string classMethod = "/staff/getdashboardpiechartitems";
            List<PieItem> items = new List<PieItem>();
            try
            {
                StaffDasboardDonutIem donutItem = new StaffDasboardDonutIem();
                DataTable resp = Utilities.EnCapriDisco_Select(staffId, "", "trueVisit_GetStaffDonutDashboardItems", "@StaffId");

                if (resp.Rows.Count == 0)
                {

                }

                foreach (DataRow row in resp.Rows)
                {
                    var total = row.Field<int>("HonoredVisits") + row.Field<int>("CancelledAppointments") + row.Field<int>("UnhonoredAppointments");
                    items.Add(new PieItem {
                        value = Convert.ToDouble(row.Field<int>("HonoredVisits")) / total * 100d,
                        label = "Honored Visits"
                    });
                    items.Add(new PieItem
                    {
                        value = Convert.ToDouble(row.Field<int>("CancelledAppointments")) / total * 100d,
                        label = "Canceled Appointments"
                    });
                    items.Add(new PieItem
                    {
                        value = Convert.ToDouble(row.Field<int>("UnhonoredAppointments")) / total * 100d,
                        label = "Unhonored Appointments"
                    });
                    
                }
                
            }
            catch (Exception ex)
            {
                //return an error page
                ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, "");
            }

            
            
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDashboarBarChartItems()
        {
            var now = DateTimeOffset.Now;
            var month1 = now.ToString("MM/yyyy");
            var month2 = now.AddMonths(-1).ToString("MM/yyyy");
            var month3 = now.AddMonths(-2).ToString("MM/yyyy");
            var month4 = now.AddMonths(-3).ToString("MM/yyyy");
            var month5 = now.AddMonths(-4).ToString("MM/yyyy");
            var month6 = now.AddMonths(-5).ToString("MM/yyyy");

            //var months = Enumerable.Range(1, 5).Select(i => now.AddMonths(-i).ToString("MM/yyyy"));
            string classMethod = "/staff/getdashboardpiechartitems";
            List<BarChartItem> items = new List<BarChartItem>();
            
            try
            {
               
                DataTable resp = Utilities.EnCapriDisco_Select(staffId, "", "GetTotalNumberOfVisitorsPerMonthForLast6MonthsByStaffId", "@StaffId");

                if (resp.Rows.Count == 0)
                {

                }

                foreach (DataRow row in resp.Rows)
                {
                    int month1_val = row.Field<int>("month1");
                    int month2_val = row.Field<int>("month2");
                    int month3_val = row.Field<int>("month3");
                    int month4_val = row.Field<int>("month4");
                    int month5_val = row.Field<int>("month5");
                    int month6_val = row.Field<int>("month6");

                    items.Add(new BarChartItem()
                    {
                        x = "This month",
                        y = month1_val
                    });

                    items.Add(new BarChartItem()
                    {
                        x = month2,
                        y = month2_val
                    });

                    items.Add(new BarChartItem()
                    {
                        x = month3,
                        y = month3_val
                    });

                    items.Add(new BarChartItem()
                    {
                        x = month4,
                        y = month4_val
                    });

                    items.Add(new BarChartItem()
                    {
                        x = month5,
                        y = month5_val
                    });

                    items.Add(new BarChartItem()
                    {
                        x = month6,
                        y = month6_val
                    });

                    


                }

            }
            catch (Exception ex)
            {
                //return an error page
                ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, "");
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }
    }


    
}
