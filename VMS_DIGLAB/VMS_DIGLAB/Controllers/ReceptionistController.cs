﻿using DigiFoodLabServiceCore;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VMS_DIGLAB.Filters;
using VMS_DIGLAB.Models;
using VMS_DIGLAB.ViewModel;
using VMSServiceCore;

namespace VMS_DIGLAB.Controllers
{
    [Authorize]
    [Receptionist]
    [ApplicationVariables]
    public class ReceptionistController : Controller
    {
        //public string staffId { get; set; }

        public string staffId
        {
            get
            {
                return Session["StaffId"].ToString();
            }
        }
        public string fullName
        {
            get
            {
                return Session["FullName"].ToString();
            }
        }
        private readonly ApplicationDbContext db = new ApplicationDbContext();
        // GET: Receptionist/Index
        public ActionResult Index()
        {
            return View();
        }

        public ReceptionistController()
        {
            //string[] staff = System.Web.HttpContext.Current.Request.LogonUserIdentity.Name.Split(new Char[] { '\\', '\\' });
            //staffId = System.Web.HttpContext.Current.Request.LogonUserIdentity.Name.Substring(13);//staff[1];

            //IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;
            //string email = user.UserPrincipalName;

            //string staffId = email.Split(new Char[] { '@' })[0];

        }

        // GET: Receptionist/SignIn
        public ActionResult SignIn()
        {
            SignInViewModel signInModel = new SignInViewModel();
            return View();
        }

        // GET: Receptionist/SignIn
        [HttpPost]
        public ActionResult SignIn(Visitor visitor)
        {
            string classMethod = "receptionist/signin";
            //visitor.AppointmentId = 1;
            string staffId = string.Empty;
            string code = "XX";
            string message = "Failure";
            try
            {
                
                if (visitor == null)
                {
                    code = "01";
                    message = "Bad input";
                }

                if (!ModelState.IsValid)
                {
                    StringBuilder strError = new StringBuilder();
                    var errors = ModelState.Values.SelectMany(x => x.Errors);

                    if (errors.Any())
                    {
                        string error = string.Empty;
                        foreach (ModelError item in errors)
                        {
                            error = string.IsNullOrEmpty(item.ErrorMessage) ? item.Exception.Message : item.ErrorMessage;
                            strError.Append(error + "|");
                        }
                        message = strError.ToString().TrimEnd('|');
                        code = "02";

                    }
                }
                else
                {
                    if (visitor.IsAppointment == true)
                    {
                        try
                        {
                            var resp = Utilities.EnCapriDisco_Select(visitor.AppointmentId, "", "GetAppointmentDetailsByAppointmentId", "@AppointmentId");

                            if (resp.Rows.Count == 0)
                            {

                            }
                            else
                            {
                                foreach (DataRow row in resp.Rows)
                                {

                                    staffId = row["StaffId"].ToString();
                                    
                                }
                            }

                            


                        }
                        catch (Exception ex)
                        {
                            ErrHandler.WriteLog("appointments/detail/" + " " + ex.Message, Utilities.GetIP());

                        }
                    }
                    else
                    {
                        staffId = visitor.HostId;
                    }
                    //create parameters to create new users
                    Dictionary<string, dynamic> sqlParameters = new Dictionary<string, dynamic>();
                    sqlParameters.Add("@StaffId", staffId);
                    sqlParameters.Add("@VisitorFullName", visitor.VisitorFullName);
                    sqlParameters.Add("@VisitorPhoneNumber", visitor.VisitorPhoneNumber);
                    sqlParameters.Add("@BranchName", visitor.BranchName);
                    sqlParameters.Add("@VisitorImage", visitor.VisitorImage);
                    sqlParameters.Add("@VisitorCompany", visitor.VisitorAddress);
                    sqlParameters.Add("@AppointmentId", visitor.IsAppointment == true?  visitor.AppointmentId : 1);
                    sqlParameters.Add("@Purpose", visitor.Purpose);
                    sqlParameters.Add("@LaptopName", visitor.LaptopName);
                    sqlParameters.Add("@SerialNumber", visitor.SerialNumber);
                    sqlParameters.Add("@Email", visitor.Email);
                    sqlParameters.Add("@IsAppointment", visitor.IsAppointment);
                    sqlParameters.Add("@TagNumber", visitor.TagNumber);
                    sqlParameters.Add("@HasDevice", visitor.HasDevice);
                    sqlParameters.Add("@DeviceType", visitor.DeviceType);
                    sqlParameters.Add("@DeviceColour", visitor.DeviceColour);
                    sqlParameters.Add("@DeviceName", visitor.DeviceName);
                    sqlParameters.Add("@VisitImage", visitor.VisitImage);

                    //call the stored procedure passing the parameters in
                    int result = Utilities.EnCapriDisco_Insert("CreateNewVisitor", sqlParameters, classMethod, staffId);

                    if (Math.Abs(result) == 0)
                    {
                        code = "03";
                        message = "Error updating the visitor record";
                    }
                    else
                    {
                        if(visitor.IsAppointment == true)
                        {
                            try
                            {

                                Dictionary<string, dynamic> newSqlParameters = new Dictionary<string, dynamic>();

                                newSqlParameters.Add("@AppointmentId", visitor.AppointmentId);
                                newSqlParameters.Add("@AppointmentAction", 2);

                                int result2 = Utilities.EnCapriDisco_Insert("ChangeAppointmentStatus", newSqlParameters, $"appointments/SIGNIN/{visitor.AppointmentId}", Utilities.GetIP());
                                if (Math.Abs(result2) > 0)
                                {
                                    code = "00";
                                    message = "Successful";
                                }
                                else
                                {
                                    code = "00";
                                    message = "Partial success";
                                }

                            }
                            catch (Exception ex)
                            {
                                ErrHandler.WriteError("Error occured updating trying to update the database", ex.Message);
                            }
                        }
                        else
                        {
                            code = "00";
                            message = "Successful";
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                ErrHandler.WriteError("Error creating a new user ---"+ ex.Message , staffId);
                code = "04";
                message = "Error occured signing visitor in the visitor record";

            }
            //ErrHandler.WriteError("Error creating a new user ---" + , staffId);
            return Json(new { code, message });
        }

        public bool IsVisitorSignedIn(string tagNumber)
        {
            try
            {
                var signedInUser = Utilities.EnCapriDisco_Select(tagNumber, "", "VisitorSignedInByTagNumber", "@TagNumber");
                if (signedInUser.Rows.Count > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {

                ErrHandler.WriteError("Error getting getting if visitor by tagnumber", ex.Message + "|" + Utilities.GetIP());
            }
            return false;
        }
        // GET: Receptionist/SignOut
        public ActionResult SignOut()
        {

            return View();
        }


        [HttpPost]
        public ActionResult SignOut(SignOutViewModel viewModel)
        {
            var classMethod = "Receptionist/SignOut";

            if (ModelState.IsValid)
            {
                try
                {
                    bool isVisitorSignedInAlready = IsVisitorSignedIn(viewModel.TagNumber);

                    if (isVisitorSignedInAlready == false)
                    {
                        ViewBag.Status = "Not Ok";
                        ViewBag.Message = "No user is not signed out with the tag number";
                    }
                    else
                    {
                        Dictionary<string, dynamic> sqlParameters = new Dictionary<string, dynamic>();
                        sqlParameters.Add("@TagNumber", viewModel.TagNumber);

                        int result = Utilities.EnCapriDisco_Insert("SignOutVisitor", sqlParameters, classMethod, viewModel.TagNumber);

                        if (Math.Abs(result) > 0)
                        {
                            ViewBag.Status = "Ok";
                            ViewBag.Message = "Visitor Signed Out Successfully";
                        }
                        else
                        {
                            ViewBag.Status = "Not Ok";
                            ViewBag.Message = "Visitor Signed Out Failed";
                        }
                    }

                    



                }
                catch (Exception ex)
                {
                    ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, "");

                }
            }

            return View(viewModel);
        }

        //Ajax to autopopulate the signOut fields

        [HttpPost]
        public ActionResult GetVisitorData(string data)
        {
            var TagNumber = data;
            string classMethod = "Visitors";

            List<Visitor> visitors = new List<Visitor>();

            try
            {
                bool isVisitorSignedInAlready = IsVisitorSignedIn(TagNumber);

                if (isVisitorSignedInAlready == false)
                {
                    ViewBag.Status = "Not Ok";
                    ViewBag.Message = "No user is not signed out with the tag number";
                }
                else
                {
                    var resp = Utilities.EnCapriDisco_Select(TagNumber, "", "GetVisitorDetailsForSignOut", "@TagNumber");
                    foreach (DataRow row in resp.Rows)
                    {
                        visitors.Add(new Visitor
                        {

                            VisitorFullName = row["VisitorFullName"].ToString(),
                            Email = row["Email"].ToString(),
                            LaptopName = row["LaptopName"].ToString(),
                            SerialNumber = row["SerialNumber"].ToString(),
                            VisitorPhoneNumber = row["VisitorPhoneNumber"].ToString()
                        }

                        );
                    }
                }

                

                // var count = visitors.Count;
            }
            catch (Exception ex)
            {
                //return an error page
                ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, "");
            }



            return Json(visitors);
        }


        [System.Web.Mvc.HttpPost]
        //[ValidateAntiForgeryToken]
        // POST: Staff/CreateInvite
        public ActionResult CreateInvite(Visitor visitor)
        {
            //get the name of currently logged user
            IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;
            //get branch name
            string userBranchName = user.PhysicalDeliveryOfficeName;
            string classMethod = "/staff/createinvite";
            string userObjectID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;

            if (ModelState.IsValid)
            {
                string[] names = visitor.VisitorName.ToString().Trim().Split(new char[]{' '}, 2);
                try
                {
                    Dictionary<string, dynamic> sqlParameters = new Dictionary<string, dynamic>();
                    sqlParameters.Add("@StaffId", staffId);
                    sqlParameters.Add("@VisitorFirstName", visitor.VisitorFirstName);
                    sqlParameters.Add("@VisitorLastName", visitor.VisitorLastName);
                    sqlParameters.Add("@VisitorPhoneNumber", visitor.VisitorPhoneNumber);
                    sqlParameters.Add("@BranchName", userBranchName);
                    sqlParameters.Add("@Purpose", visitor.Purpose);
                    sqlParameters.Add("@VisitorCompany", visitor.VisitorCompany);
                    sqlParameters.Add("@VisitTime", visitor.VisitTime); //convert this date '02-2-2019'
                    sqlParameters.Add("@VisitDate", visitor.VisitDate);  //'10:50'
                    sqlParameters.Add("@IsAppoinment", true);
                    sqlParameters.Add("@VisitStatus", 1);

                    // use this line to show it back as 24hr format 
                    //var hourPart = String.Format("{0:HH}", myDateTime)



                    int result = Utilities.EnCapriDisco_Insert("CreateNewVisitor", sqlParameters, classMethod, userObjectID);

                    if (Math.Abs(result) > 0)
                    {
                        ViewBag.Status = "00";
                        return RedirectToAction("Appointments", "Receptionist");
                    }
                    else
                    {
                        ViewBag.Status = "99";
                    }

                }
                catch (Exception ex)
                {
                    //return an error page
                    ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, "");
                    ViewBag.Status = "99";
                }

            }
            else
            {
                StringBuilder strError = new StringBuilder();
                var errors = ModelState.Values.SelectMany(x => x.Errors);

                if (errors.Any())
                {
                    string error = string.Empty;
                    foreach (ModelError item in errors)
                    {
                        error = string.IsNullOrEmpty(item.ErrorMessage) ? item.Exception.Message : item.ErrorMessage;
                        strError.Append(error + "|");
                    }

                }
                ErrHandler.WriteError(classMethod + " Invalid model for creating appointments", strError.ToString().TrimEnd('|'));
                ViewBag.Status = "XX";
            }
            //logic to create new appointment here. Handle posted data here on the CreateInvitePage
            return View(visitor);
        }



        // GET: Receptionist/Appointments
        public ActionResult Appointments()
        {
            string classMethod = "Appointments";

            //get the name of currently logged user
            IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;

            //get branch name
            string userBranchName = user.PhysicalDeliveryOfficeName;

            List<Appointment> Appointments = new List<Appointment>();

            try
            {
                /*result = db.Database.SqlQuery<VisitorViewModel>("exec GetStaffVisitors @StaffId", new SqlParameter("@StaffId", userObjectID)).ToList();*/

                //var resp = Utilities.EnCapriDisco_Select(userBranchName, "", "GetBranchAppointments", "@BranchName");

                
                    Appointments = db.Database.SqlQuery<Appointment>("exec GetBranchAppointments @BranchName", new SqlParameter("@BranchName", userBranchName)).ToList();
                    var count = Appointments.Count;
            }
            catch (Exception ex)
            {
                //return an error page
                ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, "");
            }


            //all logic to get staff appointments and return to the view
            return View(Appointments);
        }


        // GET: Receptionist/DashBoard
        public ActionResult DashBoard()
        {
            StaffDashboardViewModel dashBoardItems = new StaffDashboardViewModel();
            try
            {
                //get the name of currently logged user
                IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;

                //get branch name
                string userBranchName = user.PhysicalDeliveryOfficeName;

               
                var totalVisitors = Utilities.EnCapriDisco_Select(userBranchName, "", "GetTotalStaffVisitorsByBranchName", "@BranchName");

                if (totalVisitors.Rows.Count > 0)
                {
                    foreach (DataRow row in totalVisitors.Rows)
                    {
                        dashBoardItems.TotalVisitors = Convert.ToInt32(row["TotalVisitors"].ToString());
                    }
                }


                //get most recent staff visitors
                Dictionary<string, dynamic> sqlParameters = new Dictionary<string, dynamic>();
                sqlParameters.Add("@BranchName", userBranchName);
                sqlParameters.Add("@count", 5);

                var mostRecentVisitors = Utilities.EnCapriDisco_Select2("GetMostRecentVisitorsByBranchName", sqlParameters, "/receptionist/dashboard", userBranchName);
                List<Visitor> visitors = new List<Visitor>();
                if (mostRecentVisitors.Rows.Count > 0)
                {


                    foreach (DataRow row in mostRecentVisitors.Rows)
                    {
                        visitors.Add(
                            new Visitor
                            {
                                Email = row["Email"].ToString(),
                                VisitorFullName = row["VisitorFullName"].ToString(),
                               VisitImage = row["VisitImage"].ToString()
                            }
                        );
                    }

                }
                dashBoardItems.MostRecentVisitors = visitors;

                //get total visitors signed in today
                var totalSignedInVistorsToday = Utilities.EnCapriDisco_Select(userBranchName, "", "GetTotalStaffVisitorsSignedInTodayByBranchName", "@BranchName");

                if (totalVisitors.Rows.Count > 0)
                {
                    foreach (DataRow row in totalSignedInVistorsToday.Rows)
                    {
                        dashBoardItems.TotalSignedInVisitorToday = Convert.ToInt32(row["StaffVisitorCount"].ToString());
                    }
                }


                //get total appointments today
                DataTable totalAppointmentsToday = Utilities.EnCapriDisco_Select(userBranchName, "", "GetTotalStaffVisitorsOnAppointmentTodayByBranchName", "@BranchName");

                if (totalAppointmentsToday.Rows.Count > 0)
                {
                    foreach (DataRow row in totalAppointmentsToday.Rows)
                    {
                        dashBoardItems.TotalAppointmentsToday = Convert.ToInt32(row["StaffVisitorCount"].ToString());
                    }
                }
                

            }
            catch (Exception ex)
            {
                ErrHandler.WriteError("Error getting dashboard items", ex.Message + "|" + Utilities.GetIP());
            }




            return View(dashBoardItems);
        }

        // GET: Receptionist/CreateInvite
        public ActionResult CreateInvite()
        {
            return View();
        }





        public ActionResult VisitorLog()
        {
            string classMethod = "/Receptionist/VisitorLog";
            List<VisitorViewModel> visitors = new List<VisitorViewModel>();

            IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;

            string userBranchName = user.PhysicalDeliveryOfficeName;

            ErrHandler.WriteLog("User is about to view all his visitor records", userBranchName);

            List<VisitorViewModel> result = new List<VisitorViewModel>();

            try
            {

                var resp = Utilities.EnCapriDisco_Select(userBranchName, "", "GetBranchVisitorsByBranchName", "@BranchName");

                foreach (DataRow row in resp.Rows)
                {
                    visitors.Add(new VisitorViewModel
                    {

                        BranchName = row["BranchName"].ToString(),
                        VisitorCompany = row["VisitorCompany"].ToString(),
                        Email = row["Email"].ToString(),
                        VisitorFirstName = row["VisitorFullName"].ToString(),

                        VisitorPhoneNumber = row["VisitorPhoneNumber"].ToString(),
                        TimeIn = row.Field<DateTime>("TimeIn"),
                        TimeOut = row.Field<DateTime?>("TimeOut"),
                        Purpose = (AppointmentPurpose)Convert.ToInt32(row["Purpose"].ToString()),
                        VisitImage = row["VisitImage"].ToString()
                    }

                    //must return for appointment
                    //must reyrun for visitor
                    // VisitorName, VisitorCompany, Email, VisitorPhoneNumber,-- AppointmentTime, Purpose, TimeIn, TimeOut
                    );
                }

                var count = result.Count;
            }
            catch (Exception ex)
            {
                //return an error page
                ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, "");
            }

            return View(visitors);
        }


        public JsonResult PendingAppointments(string staffId)
        {
            string code = "xx";
            string message = string.Empty;
            List<Appointment> appointments = new List<Appointment>();
            if (string.IsNullOrEmpty(staffId) || !staffId.All(c => Char.IsLetterOrDigit(c)))
            {
                message = "Invalid StaffId";
            }


            else
            {
                
                try
                {
                    var resp = Utilities.EnCapriDisco_Select(staffId, "", "GetBranchPendingAppointments", "@StaffId");
                    if (resp.Rows.Count > 0)
                    {
                        foreach (DataRow row in resp.Rows)
                        {
                            appointments.Add(new Appointment
                            {

                                VisitorCompany = row["VisitorCompany"].ToString(),
                                Email = row["Email"].ToString(),
                                VisitorFullName = row["VisitorFullName"].ToString(),
                                Purpose = (AppointmentPurpose)row.Field<int>("Purpose"),
                                VisitorPhoneNumber = row["VisitorPhoneNumber"].ToString(),
                                AppointmentId = row.Field<int>("AppointmentId"),
                                BranchName = row["BranchName"].ToString()
                            });
                        }
                        code = "00";
                        message = "Successful";
                    }
                    else
                    {
                        code = "99";
                        message = "Could not get appointments for user";
                    }
                }
                catch (Exception ex)
                {
                    ErrHandler.WriteError("/receptionist/getbranchappointments" + " Error getting branch users. " + ex.Message, "");

                }

            }



            return Json(new { appointments, code, message }, JsonRequestBehavior.AllowGet);
        }

        [Route("receptionist/appointmentdetail/{id}")]
        public JsonResult AppointmentDetail(int id)
        {
            List<Appointment> appointments = new List<Appointment>()
            {
                //get pendng appointments
                new Appointment
                {
                    AppointmentTime = DateTime.Now,
                    AppointmentId = 3,
                    VisitorPhoneNumber = "09038969154",
                    Purpose = AppointmentPurpose.Official,
                    Email   = "a@b.com",
                    VisitorCompany  = "DipoleDiamond",
                    VisitorFullName = "Aribo Oluwatoba"
                },
                new Appointment
                {
                    AppointmentTime = DateTime.Now,
                    AppointmentId = 2,
                    VisitorPhoneNumber = "09038969154",
                    Purpose = AppointmentPurpose.Official,
                    Email   = "a@b.com",
                    VisitorCompany  = "DipoleDiamond",
                    VisitorFullName = "Sayo Tempa"
                }
            };
            Appointment appointment = new Appointment();
            appointment = appointments.FirstOrDefault(o => o.AppointmentId == id);

            return Json(appointment, JsonRequestBehavior.AllowGet);
        }

        [Route("receptionist/branchusers")]
        public JsonResult BranchUsers()
        {
            string classMethod = "receptionist/branchusers";
            //get users in receptionist branch and return the users 
            List<Staff> staff = new List<Staff>();
            try
            {
                var resp = Utilities.EnCapriDisco_Select("test", "", "GetAllBranchUsers", "@branch");
                if (resp.Rows.Count > 0)
                {
                    foreach (DataRow row in resp.Rows)
                    {
                        staff.Add(new Staff {
                            StaffId = row["staff_ID"].ToString(),
                            FullName = row["staff_name"].ToString(),
                            Unit = row["branch_name"].ToString(),
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                ErrHandler.WriteError(classMethod + " Error getting branch users. " + ex.Message, "");

            }
            
            return Json(staff, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetDashboardPieChartItems()
        {
            string classMethod = "/receptionist/getdashboardpiechartitems";
            List<PieItem> items = new List<PieItem>();
            try
            {
                StaffDasboardDonutIem donutItem = new StaffDasboardDonutIem();
                DataTable resp = Utilities.EnCapriDisco_Select("test", "", "trueVisit_GetReceptionistDashboardItems", "@branchname");

                if (resp.Rows.Count == 0)
                {

                }

                foreach (DataRow row in resp.Rows)
                {
                    var total = row.Field<int>("BlockA") + row.Field<int>("BlockB") + row.Field<int>("BlockC") + row.Field<int>("FidelityPlace") + row.Field<int>("HrOffice");
                    items.Add(new PieItem
                    {
                        value = Math.Round(Convert.ToDouble(row.Field<int>("BlockA")) / total * 100d, 2),
                        label = "Block A"
                    });
                    items.Add(new PieItem
                    {
                        value = Math.Round(Convert.ToDouble(row.Field<int>("BlockB")) / total * 100d, 2),
                        label = "Block B"
                    });
                    items.Add(new PieItem
                    {
                        value = Math.Round(Convert.ToDouble(row.Field<int>("BlockC")) / total * 100d, 2),
                        label = "Block C"
                    });
                    items.Add(new PieItem
                    {
                        value = Math.Round(Convert.ToDouble(row.Field<int>("FidelityPlace")) / total * 100d, 2),
                        label = "Fidelity Place"
                    });
                    items.Add(new PieItem
                    {
                        value = Math.Round(Convert.ToDouble(row.Field<int>("HrOffice")) / total * 100d, 2),
                        label = "HR Office"
                    });

                }

            }
            catch (Exception ex)
            {
                //return an error page
                ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, "");
            }



            return Json(items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDashboarBarChartItems()
        {
            var now = DateTimeOffset.Now;
            var month1 = now.ToString("MM/yyyy");
            var month2 = now.AddMonths(-1).ToString("MM/yyyy");
            var month3 = now.AddMonths(-2).ToString("MM/yyyy");
            var month4 = now.AddMonths(-3).ToString("MM/yyyy");
            var month5 = now.AddMonths(-4).ToString("MM/yyyy");
            var month6 = now.AddMonths(-5).ToString("MM/yyyy");

            //var months = Enumerable.Range(1, 5).Select(i => now.AddMonths(-i).ToString("MM/yyyy"));
            string classMethod = "/receptionist/getdashboardpiechartitems";
            List<BarChartItem> items = new List<BarChartItem>();

            try
            {

                DataTable resp = Utilities.EnCapriDisco_Select("test", "", "GetTotalNumberOfVisitorsPerMonthForLast6MonthsByBranchName", "@branchname");

                if (resp.Rows.Count == 0)
                {

                }

                foreach (DataRow row in resp.Rows)
                {
                    int month1_val = row.Field<int>("month1");
                    int month2_val = row.Field<int>("month2");
                    int month3_val = row.Field<int>("month3");
                    int month4_val = row.Field<int>("month4");
                    int month5_val = row.Field<int>("month5");
                    int month6_val = row.Field<int>("month6");

                    items.Add(new BarChartItem()
                    {
                        x = "This month",
                        y = month1_val
                    });

                    items.Add(new BarChartItem()
                    {
                        x = month2,
                        y = month2_val
                    });

                    items.Add(new BarChartItem()
                    {
                        x = month3,
                        y = month3_val
                    });

                    items.Add(new BarChartItem()
                    {
                        x = month4,
                        y = month4_val
                    });

                    items.Add(new BarChartItem()
                    {
                        x = month5,
                        y = month5_val
                    });

                    items.Add(new BarChartItem()
                    {
                        x = month6,
                        y = month6_val
                    });




                }

            }
            catch (Exception ex)
            {
                //return an error page
                ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, "");
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUserAttendanceRecord(string staffId)
        {
            string code = "xx";
            string message = string.Empty;
            List<AttendanceRecord> attendanceRecords = new List<AttendanceRecord>();
            if (string.IsNullOrEmpty(staffId) || !staffId.All(c => Char.IsLetterOrDigit(c)))
            {
                message = "Invalid StaffId";
            }


            else
            {

                try
                {
                    var resp = Utilities.EnCapriDisco_Select(staffId, "", "AttendanceManagement_GetIndividualAttendanceHistory", "@UserId"); //replace the staffId variable with the staffId you got in your application
                    if (resp.Rows.Count > 0)
                    {
                        foreach (DataRow row in resp.Rows)
                        {
                            attendanceRecords.Add(new AttendanceRecord
                            {

                                Date = row.Field<DateTime>("Purpose").ToString("yyyy/MM/dd"),
                                TimeIn = row["TimeIn"].ToString(),
                                TimeOut = row["TimeOut"].ToString()
                            });
                        }
                        code = "00";
                        message = "Successful";
                    }
                    else
                    {
                        code = "99";
                        message = "Could not get your logs";
                    }
                }
                catch (Exception ex)
                {
                    ErrHandler.WriteError("/receptionist/getbranchappointments" + " Error getting  attendance log. " + ex.Message, "");

                }

            }



            return Json(new { attendanceRecords, code, message }, JsonRequestBehavior.AllowGet);
        }
    }

    public class AttendanceRecord
    {
        public string Date { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public string Remark { get; set; }
    }
}
