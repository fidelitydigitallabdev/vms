﻿using DigiFoodLabServiceCore;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Web.Mvc;
using VMS_DIGLAB.Filters;
using VMS_DIGLAB.Models;
using VMS_DIGLAB.ViewModel;
using VMSServiceCore;

namespace VMS_DIGLAB.Controllers
{
    [Authorize]
    [Admin]
    [ApplicationVariables]
    public class AdminController : Controller
    {
        //public string staffId { get; set; }

        public string staffId
        {
            get
            {
                return Session["StaffId"].ToString();
            }
        }
        public string fullName
        {
            get
            {
                return Session["FullName"].ToString();
            }
        }

        //Get Index
        public ActionResult Index()
        {
            return RedirectToAction("DashBoard");
        }

        
        public AdminController()
        {
            IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;
            string email = user.UserPrincipalName;

            string staffId = email.Split(new Char[] { '@' })[0];

        }

        //GET Admin/DashBoard

        public ActionResult DashBoard()
        {
            StaffDashboardViewModel dashBoardItems = new StaffDashboardViewModel();
            try
            {
                string classMethod = "/Admin/Dashboard";

                //get total visitor count

                Dictionary<string, dynamic> sqlParameters = new Dictionary<string, dynamic>();
                var totalVisitors = Utilities.EnCapriDisco_Select2("GetTotalVisitors", sqlParameters, classMethod, "");
                // var totalVisitors = Utilities.EnCapriDisco_Select2("GetTotalVisitors", "@BranchName");

                if (totalVisitors.Rows.Count > 0)
                {
                    foreach (DataRow row in totalVisitors.Rows)
                    {
                        dashBoardItems.TotalVisitors = Convert.ToInt32(row["TotalVisitors"].ToString());
                    }
                }


                //get most recent staff visitors

                sqlParameters.Add("@count", 5);

                var mostRecentVisitors = Utilities.EnCapriDisco_Select2("GetAllMostRecentVisitors", sqlParameters, classMethod, "");


                List<Visitor> visitors = new List<Visitor>();
                if (mostRecentVisitors.Rows.Count > 0)
                {


                    foreach (DataRow row in mostRecentVisitors.Rows)
                    {
                        visitors.Add(
                            new Visitor
                            {
                                Email = row["Email"].ToString(),
                                VisitorFullName = row["VisitorFullName"].ToString(),
                                VisitImage = row["VisitImage"].ToString()
                            }
                        );
                    }

                }
                dashBoardItems.MostRecentVisitors = visitors;

                //get total visitors signed in today
                Dictionary<string, dynamic> sqlParameters2 = new Dictionary<string, dynamic>();
                var totalSignedInVistorsToday = Utilities.EnCapriDisco_Select2("GetTotalVisitorsSignedInToday", sqlParameters2, classMethod, "");


                if (totalVisitors.Rows.Count > 0)
                {
                    foreach (DataRow row in totalSignedInVistorsToday.Rows)
                    {
                        dashBoardItems.TotalSignedInVisitorToday = Convert.ToInt32(row["StaffVisitorCount"].ToString());
                    }
                }


                //get total appointments today

                DataTable totalAppointmentsToday = Utilities.EnCapriDisco_Select2("GetTotalVisitorsOnAppointmentToday", sqlParameters2, classMethod, "");


                if (totalAppointmentsToday.Rows.Count > 0)
                {
                    foreach (DataRow row in totalAppointmentsToday.Rows)
                    {
                        dashBoardItems.TotalAppointmentsToday = Convert.ToInt32(row["StaffVisitorCount"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrHandler.WriteError("Error getting dashboard items", ex.Message + "|" + Utilities.GetIP());
            }




            return View(dashBoardItems);
        }
        // GET: Receptionist/CreateInvite
        public ActionResult CreateInvite()
        {
            return View();
        }

        // Admin/VisitorLog
        public ActionResult VisitorLog()
        {
            string classMethod = "/Admin/VisitorLog";

            List<VisitorViewModel> visitors = new List<VisitorViewModel>();



            try
            {
                Dictionary<string, dynamic> sqlParameters = new Dictionary<string, dynamic>();
                var resp = Utilities.EnCapriDisco_Select2("GetAllVisitors", sqlParameters, classMethod, "");

                foreach (DataRow row in resp.Rows)
                {
                    visitors.Add(new VisitorViewModel
                    {
                        //BranchName = row["BranchName"].ToString(),
                        VisitorCompany = row["VisitorCompany"].ToString(),
                        Email = row["Email"].ToString(),
                        VisitorFullName = row["VisitorFullName"].ToString(),
                        VisitorPhoneNumber = row["VisitorPhoneNumber"].ToString(),
                        //VisitTime = row["VisitTime"].ToString(),
                        TimeIn = row.Field<DateTime>("TimeIn"),
                        TimeOut = row.Field<DateTime?>("TimeOut"),
                        Purpose = (AppointmentPurpose)Convert.ToInt32(row["Purpose"].ToString()),
                        VisitImage = row["VisitImage"].ToString()
                    }
                    
                    );
                }

                var count = visitors.Count;
            }
            catch (Exception ex)
            {
                //return an error page
                ErrHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, "");
            }

            return View(visitors);
        }
    }
}
