﻿using DigiFoodLabServiceCore;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.Routing;
using VMS_DIGLAB.Models;
using VMSServiceCore;

namespace VMS_DIGLAB.Filters
{
    public class Receptionist : System.Web.Mvc.ActionFilterAttribute
    {
        
        private static string authorizedReceptionists = ConfigurationManager.AppSettings["Receptionists"];
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ErrHandler.WriteLog("user about to get to the receptionist controller", "");
            try
            {
                //string[] staff= HttpContext.Current.Request.LogonUserIdentity.Name.Split(new Char[] { '\\', '\\' });
                //string staffId = HttpContext.Current.Request.LogonUserIdentity.Name.Substring(13);
                IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;
                string email = user.UserPrincipalName;

                string staffId = email.Split(new Char[] { '@' })[0];

                if (!authorizedReceptionists.Contains(staffId))
                {
                   
                    filterContext.Result = new HttpUnauthorizedResult();//new RedirectResult(string.Format("/Account/SignIn", filterContext.HttpContext.Request.Url.AbsolutePath));
                }
            }
            catch (Exception ex)
            {

                ErrHandler.WriteError("Error getting if staff is a receptionist|", ex.Message );
            }
            
           
        }

    }

    public class LandingPage : System.Web.Mvc.ActionFilterAttribute
    {
        private static string authorizedReceptionists = ConfigurationManager.AppSettings["Receptionists"];
        private static string authorizedAdmins = ConfigurationManager.AppSettings["Admins"];
        private static string addUrl = ConfigurationManager.AppSettings["AddUrl"] ;
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                addUrl = addUrl == "" ? addUrl : "/"+addUrl ;
                var now = DateTimeOffset.Now;
                var month1 = now.AddMonths(-1).ToString("MM/yyyy");
                var months = Enumerable.Range(1, 5).Select(i => now.AddMonths(-i).ToString("MM/yyyy"));
                ErrHandler.WriteLog("user about to get to the landing page", "");
                //string[] staff = HttpContext.Current.Request.LogonUserIdentity.Name.Split(new Char[] { '\\', '\\' });
                //string staffId = HttpContext.Current.Request.LogonUserIdentity.Name.Substring(13); //staff[1];

                IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;
                string email = user.UserPrincipalName;

                string staffId = email.Split(new Char[] { '@' })[0];

                
                ErrHandler.WriteLog("Got staff id", staffId);

                if (authorizedReceptionists.Contains(staffId))
                {
                    filterContext.Result = new RedirectToRouteResult( new RouteValueDictionary(new { controller = "Receptionist", action = "Index" }));

                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                    //filterContext.Result = new RedirectResult(string.Format("/vms_publish/Receptionist/", ""));
                }
                else if (authorizedAdmins.Contains(staffId))
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Admin", action = "Index" }));

                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                    //filterContext.Result = new RedirectResult(string.Format("/vms_publish/admin/", ""));
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Staff", action = "Index" }));

                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                    //filterContext.Result = new RedirectResult(string.Format("/vms_publish/staff/", ""));
                }
            }
            catch (Exception ex)
            {

                ErrHandler.WriteError("Error Determining receptionist" + ex.Message, "");
            }
            

        }

    }
    public class Admin : System.Web.Mvc.ActionFilterAttribute
    {
        private static string authorizedAdmins = ConfigurationManager.AppSettings["Admins"];
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                ErrHandler.WriteLog("user about to get to the admin controller", "");
                //string[] staff = HttpContext.Current.Request.LogonUserIdentity.Name.Split(new Char[] { '\\', '\\' });
                //string staffId = HttpContext.Current.Request.LogonUserIdentity.Name.Substring(13);//staff[1];
                IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;
                string email = user.UserPrincipalName;

                string staffId = email.Split(new Char[] { '@' })[0];

                ErrHandler.WriteLog("Got staff id", staffId);

                if (!authorizedAdmins.Contains(staffId))
                {
                    filterContext.Result = new HttpUnauthorizedResult();//new //RedirectResult(string.Format("/Account/SignIn", filterContext.HttpContext.Request.Url.AbsolutePath));
                }
            }
            catch (Exception ex)
            {

                ErrHandler.WriteError("Error Determining admin|" + ex.Message, "");
            }
            

        }
    }

    public class FidelityStaff : System.Web.Mvc.ActionFilterAttribute
    {
        private static string authorizedAdmins = ConfigurationManager.AppSettings["Admins"];
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ErrHandler.WriteLog("user about to get to the staff controller", "");
            //string[] staff = HttpContext.Current.Request.LogonUserIdentity.Name.Split(new Char[] { '\\', '\\' });
            //string staffId = HttpContext.Current.Request.LogonUserIdentity.Name.Substring(13);//staff[1];
            IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;
            string email = user.UserPrincipalName;

            string staffId = email.Split(new Char[] { '@' })[0];

            try
            {
                var userRow = Utilities.EnCapriDisco_Select(staffId, "", "CheckStaffExist", "@StaffId");
                int userExists = 0;
                
                if (userRow.Rows.Count > 0)
                {
                    foreach (DataRow row in userRow.Rows)
                    {
                        userExists = Convert.ToInt32(row["STAFFCOUNT"].ToString());
                        ErrHandler.WriteLog("check if user exist in the db---------"+ HttpContext.Current.Request.LogonUserIdentity.Name, userExists.ToString() + "returned");
                    }

                    if (userExists == 0)
                    {
                        ErrHandler.WriteLog("user is unauthorized---------", staffId.ToString());
                        filterContext.Result = new HttpUnauthorizedResult();
                    }
                }
                else
                {
                    ErrHandler.WriteError("Error Determining staff", staffId);
                    filterContext.Result = new HttpUnauthorizedResult();
                }
            }
            catch (Exception ex)
            {

                ErrHandler.WriteError("Error Determining staff|" +ex.Message, staffId);
            }

            

        }
    }

    public class ApplicationVariables : System.Web.Mvc.ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                var pix = filterContext.HttpContext.Session["Thunbnail"];
                var staffId = filterContext.HttpContext.Session["StaffId"];
                var fullName = string.Empty;
                IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;
                if (staffId == null)
                {
                    
                    string email = user.UserPrincipalName;
                    filterContext.HttpContext.Session["StaffId"] = email.Split(new Char[] { '@' })[0];
                    filterContext.HttpContext.Session["FullName"] = user.DisplayName;
                    fullName = user.DisplayName;
                }
                
                if (user != null)
                {
                    filterContext.HttpContext.Session["JobTitle"] = user.JobTitle;
                    filterContext.HttpContext.Session["Nickname"] = user.MailNickname;
                }

                if (pix == null)
                {
                    string userThumbnail = Task.Run(() => AdService.GetProfilePhoto()).Result;
                    

                    if (string.IsNullOrEmpty(userThumbnail))
                    {
                        var array = fullName.Split(' ');
                        string result = string.Join(".", array).ToUpper();

                    }
                    else
                    {
                        filterContext.HttpContext.Session["Thunbnail"] = userThumbnail;
                        
                    }
                    
                }

                
                
            }
            catch (Exception ex)
            {

                ErrHandler.WriteError("Error trying to set the user variables" + ex.Message, "");
            }
            
        }
    }
}