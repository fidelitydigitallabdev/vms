﻿namespace VMS_DIGLAB.Models
{
    public class User
    {
        public string UserId { get; set; }
        public string StaffId { get; set; }
        public string FullName { get; set; }
    }
}