﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VMS_DIGLAB.Models
{
    public class Visitor : VisitorDetails
    {
        public int VisitorId { get; set; }
        public string VisitImage { get; set; } //test
        [Required]
        public DateTime VisitDate { get; set; }
        public DateTime VisitTime { get; set; }
        
        [Required]
        public byte[] VisitorImage { get; set; }

        
        public int? VisitStatus { get; set; }

        [Required]
        public bool IsAppointment { get; set; }

        [Required]
        public string TagNumber { get; set; }


        public string LaptopName { get; set; }

        public string SerialNumber { get; set; }
        public DateTime TimeIn { get; set; }
        public DateTime? TimeOut { get; set; }


        public string HasDevice { get; set; }

        public string DeviceType { get; set; }
        public string DeviceColour { get; set; }
        public string DeviceName { get; set; }
        public string VisitorAddress { get; set; }

    }

    public class DeviceDetail
    {
        public string HasDevice { get; set; }

        public string DeviceType { get; set; }
        public string DeviceColour { get; set; }
        public string DeviceName { get; set; }
        public string SerialNumber { get; set; }
    }

}