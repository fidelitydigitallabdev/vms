﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS_DIGLAB.Models.API
{
    public class CreateInviteResponse
    {
        public string ResponseCode { get; set; }
        public string ResponseDescription { get; set; }
    }
}