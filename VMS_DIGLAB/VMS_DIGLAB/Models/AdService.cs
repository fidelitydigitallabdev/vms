﻿using DigiFoodLabServiceCore;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using Microsoft.Azure.ActiveDirectory.GraphClient.Extensions;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using VMSServiceCore;

namespace VMS_DIGLAB.Models
{
    public static class AdService
    {
        private static ApplicationDbContext db = new ApplicationDbContext();
        private static string clientId = ConfigurationManager.AppSettings["ida:ClientId"];
        private static string appKey = ConfigurationManager.AppSettings["ida:ClientSecret"];
        private static string aadInstance = EnsureTrailingSlash(ConfigurationManager.AppSettings["ida:AADInstance"]);
        private static string graphResourceID = "https://graph.windows.net";

        // GET: UserProfile
        public static async Task<IUser> GetCurrentUser()
        {
            
            try
            {
                string tenantID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;
                string userObjectID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;
                Uri servicePointUri = new Uri(graphResourceID);
                Uri serviceRoot = new Uri(servicePointUri, tenantID);
                ActiveDirectoryClient activeDirectoryClient = new ActiveDirectoryClient(serviceRoot,
                      async () => await GetTokenForApplication());

                // use the token for querying the graph to get the user details

                var result = await activeDirectoryClient.Users
                    .Where(u => u.ObjectId.Equals(userObjectID))
                    .ExecuteAsync();
                IUser user = result.CurrentPage.ToList().First();

                return user;
            }
            catch (AdalException ex)
            {
                // Return to error page.
                ErrHandler.WriteError("Error getting current user------ "+ex.Message,"At adservice/getcurrentuser/ADAL exception" );
                throw;
            }

            // if the above failed, the user needs to explicitly re-authenticate for the app to obtain the required token
            catch (Exception ex)
            {
                ErrHandler.WriteError("Error getting current user------ " + ex.Message, "At adservice/getcurrentuser/ exception");
                var currentUri = HttpContext.Current.Request.Url.AbsoluteUri;
                
                throw;
            }
        }
        public static async Task<IList<IUser>> GetAllUsers(string jobtitle)
        {
            string tenantID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;
            string userObjectID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;
            try
            {
                Uri servicePointUri = new Uri(graphResourceID);
                Uri serviceRoot = new Uri(servicePointUri, tenantID);
                ActiveDirectoryClient activeDirectoryClient = new ActiveDirectoryClient(serviceRoot,
                      async () => await GetTokenForApplication());

                // use the token for querying the graph to get the user details

                var result = await activeDirectoryClient.Users
                    .Where(u => u.JobTitle == jobtitle)
                    .ExecuteAsync();
                IList<IUser> user = result.CurrentPage.ToList();

                return user;
            }
            catch (AdalException)
            {
                // Return to error page.
                throw;
            }

            // if the above failed, the user needs to explicitly re-authenticate for the app to obtain the required token
            catch (Exception)
            {
                throw;
            }
        }

        //public void RefreshSession()
        //{
        //    HttpContext.GetOwinContext().Authentication.Challenge(
        //        new AuthenticationProperties { RedirectUri = "/UserProfile" },
        //        OpenIdConnectAuthenticationDefaults.AuthenticationType);
        //}

        public static async Task<string> GetTokenForApplication()
        {
            string signedInUserID = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
            string tenantID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;
            string userObjectID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;

            // get a token for the Graph without triggering any user interaction (from the cache, via multi-resource refresh token, etc)
            ClientCredential clientcred = new ClientCredential(clientId, appKey);
            // initialize AuthenticationContext with the token cache of the currently signed in user, as kept in the app's database
            AuthenticationContext authenticationContext = new AuthenticationContext(aadInstance + tenantID, new ADALTokenCache(signedInUserID));
            AuthenticationResult authenticationResult = await authenticationContext.AcquireTokenSilentAsync(graphResourceID, clientcred, new UserIdentifier(userObjectID, UserIdentifierType.UniqueId));
            return authenticationResult.AccessToken;
        }

        private static string EnsureTrailingSlash(string value)
        {
            if (value == null)
            {
                value = string.Empty;
            }

            if (!value.EndsWith("/", StringComparison.Ordinal))
            {
                return value + "/";
            }

            return value;
        }


        public static  async Task<List<Staff>> GetAllUsers()
        {
            string tenantID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;
            string userObjectID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;
            List<Staff> listUsers = new List<Staff>();
            try
            {
                Uri servicePointUri = new Uri(graphResourceID);
                Uri serviceRoot = new Uri(servicePointUri, tenantID);
                ActiveDirectoryClient activeDirectoryClient = new ActiveDirectoryClient(serviceRoot,
                      async () => await GetTokenForApplication());
                
                // use the token for querying the graph to get the user details

                Task<IPagedCollection<IUser>> usersTask = activeDirectoryClient.Users.ExecuteAsync();
                IPagedCollection<IUser> usersA = await usersTask;
                List<IUser> queryUsers = new List<IUser>();
                

                do
                {
                    List<IUser> queryUsersList = usersA.CurrentPage.ToList();
                    queryUsers.AddRange(queryUsersList);
                    usersA = usersA.MorePagesAvailable ? await usersA.GetNextPageAsync() : null;
                }
                while (usersA != null);

                if (queryUsers.Count > 0)
                {
                    listUsers = queryUsers.Select(u => new Staff { Name = u.DisplayName, UName = u.UserPrincipalName }).ToList();
                }

                return listUsers;

            }
            catch (AdalException)
            {
                // Return to error page.
                throw;
            }

            // if the above failed, the user needs to explicitly re-authenticate for the app to obtain the required token
            catch (Exception)
            {

            }
            
            return listUsers;
        }


        public static async Task<string> GetProfilePhoto()
        {
            string tenantID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;
            string userObjectID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;
            string imageString = string.Empty;
            //userObjectID = "98e0c391-aa3e-4812-a5d3-37bdbfd3924b";
            using (HttpClient c = new HttpClient())
            {
                try
                {
                    //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenForApplication());
                    //contentBytes = await client.GetByteArrayAsync(new Uri($"{graphResourceID}/{tenantID}/directoryObjects/{userObjectID}/Microsoft.DirectoryServices.User/thumbnailPhoto?api-version=1.6"));
                    
                    
                    var resource = $"/{ tenantID}/directoryObjects/{userObjectID}/Microsoft.DirectoryServices.User/thumbnailPhoto?api-version=1.6";
                    c.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenForApplication());
                    imageString = await c.GetStringAsync(graphResourceID + resource);
                    //c.BaseAddress = new Uri(graphResourceID+resource);
                    
                }
                catch (Exception ex)
                {
                    ErrHandler.WriteError("Error getting profile photo----", ex.Message);
                }



            }
            return imageString;
        }

        public static string GetStaffNo()
        {
            string staffId = string.Empty;
            try
            {
                IUser user = Task.Run(() => AdService.GetCurrentUser()).Result;
                string email = user.UserPrincipalName;

                staffId =  email.Split(new Char[] { '@' })[0];
            }
            catch (Exception ex)
            {

                ErrHandler.WriteError("Error getting staff id", ex.Message + "|" + Utilities.GetIP());
            }
            return staffId;
        }

    }
}