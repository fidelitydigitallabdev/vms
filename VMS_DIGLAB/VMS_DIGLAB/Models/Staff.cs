﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS_DIGLAB.Models
{
    public class Staff
    {
        public string Name { get; set; }
        public string UserId { get; set; }
        public string StaffId { get; set; }
        public string FullName { get; set; }
        public string UName { get; set; }
        public string Unit { get; set; }
        public Byte[] Image { get; set; }
    }
}