﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VMS_DIGLAB.Models
{
    public class Appointment : VisitorDetails
    {
        //[Required]
        public DateTime? AppointmentDate { get; set; }

        public string ArrivalDate { get; set; }
        //[Required]
        public DateTime? AppointmentTime { get; set; }

        public string ArrivalTime { get; set; }

        //[Required]
        public Staff Host { get; set; }

        public AppointmentStatus AppointmentStatus { get; set; }

        public string HostId { get; set; }
    }

    public class VisitorDetails
    {
        //[Required]
        [MaxLength(50, ErrorMessage = "Firstname cannot be more than 50 characters long")]
        public string VisitorFirstName { get; set; }

        //[Required]
        [MaxLength(50, ErrorMessage = "Last name cannot be more than 50 characters long")]
        public string VisitorLastName { get; set; }


        [Required]
        //[DataType(DataType.PhoneNumber, ErrorMessage = " Please provide a valid phone number")]
        public string VisitorPhoneNumber { get; set; }

        //[Required]
        [MaxLength(50, ErrorMessage = "Branch name length exceeded limit of 100 characters")]
        public string BranchName { get; set; }

        public AppointmentPurpose Purpose { get; set; }

        public int? AppointmentId { get; set; }

        [Required]
        public string VisitorCompany { get; set; }

        public string Email { get; set; }

        public string VisitorName
        {
            get { return VisitorFirstName + " " + VisitorLastName; }
        }
        public string VisitorFullName { get; set; }
        public string  HostId { get; set; }

    }


    public enum AppointmentPurpose
    {
        Personal = 1, Official, Other
    }
    public enum AppointmentStatus
    {
        Pending = 1, Visited, Canceled
    }
}