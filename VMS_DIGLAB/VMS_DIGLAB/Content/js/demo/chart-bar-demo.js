$(document).ready(function () {
    // Set new default font family and font color to mimic Bootstrap's default styling
    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#292b2c';

    // Bar Chart Example
    var ctx = document.getElementById("myBarChart");
    var employeeNames = getFieldValues('employee_name');
    var employeeCount = getFieldValues('visitor_count');

    
    var myLineChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels: employeeNames,//["January", "February", "March", "April", "May", "June"],
        datasets: [{
          label: "Visitors",
          backgroundColor: "rgba(2,117,216,1)",
          borderColor: "rgba(2,117,216,1)",
            data: employeeCount,//[10000, 8312, 5251, 9841, 12821, 11984],
        }],
      },
      options: {
        scales: {
          xAxes: [{
            time: {
              unit: 'month'
            },
            gridLines: {
              display: false
            },
            ticks: {
              maxTicksLimit: 6
            }
          }],
          yAxes: [{
            ticks: {
              min: 0,
              max: 100,
              maxTicksLimit: 5
            },
            gridLines: {
              display: true
            }
          }],
        },
        legend: {
          display: false
        }
      }
    });
    

    function getFieldValues(fieldId) {
        //get the employee name
        var fieldValues = [];
        console.log('Got here');
        //console.log('tbody tr td' + fieldId);
        $('tbody tr td.' + fieldId).each(function (index) {
            console.log(index);
            fieldValues.push($(this).text());
        });

        return fieldValues;
    }



    


});